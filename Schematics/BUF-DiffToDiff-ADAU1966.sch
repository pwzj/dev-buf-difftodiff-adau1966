EESchema Schematic File Version 2
LIBS:power
LIBS:MyKiCadLibs-Lib
LIBS:BUF-DiffToDiff-ADAU1966-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 3
Title "OHDSP BUF-DiffToDiff-ADAU1966"
Date "22 Jan 16"
Rev "1.0"
Comp ""
Comment1 "MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE."
Comment2 "is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF"
Comment3 "Licensed under the TAPR Open Hardware License (www.tapr.org/OHL). This documentation"
Comment4 "Copyright Paul Janicki 2016"
$EndDescr
$Sheet
S 3350 3150 1800 1050
U 562E6585
F0 "PowerSupply" 60
F1 "PowerSupply.sch" 60
$EndSheet
$Sheet
S 6400 2000 1950 3700
U 5633C57F
F0 "AnaloguePassiveFilters" 60
F1 "AnaloguePassiveFilters.sch" 60
$EndSheet
Text Notes 900  950  0    118  ~ 0
OHDSP Differential Active DAC Output Buffer for ADAU1966
$EndSCHEMATC
